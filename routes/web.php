<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => '/home'], function(){
    Route::get('/', 'HomeController@index')->name('home');
    Route::post('/search', 'HomeController@search')->name('home.search');
});


Route::group(['prefix' => '/company'], function(){
    Route::get('/index', 'CompanyController@index')->name('company.index');
    Route::post('/create', 'CompanyController@create')->name('company.create');
    Route::get('/delete/{id}', 'CompanyController@delete')->name('company.delete');
});
Route::group(['prefix' => '/driver'], function(){
    Route::get('/index', 'DriverController@index')->name('driver.index');
    Route::post('/create', 'DriverController@create')->name('driver.create');
    Route::get('/delete/{id}', 'DriverController@delete')->name('driver.delete');
});

Route::group(['prefix' => '/training'], function(){
    Route::get('/{id}', 'TrainingController@index')->name('training');
    Route::post('/finish/{id}', 'TrainingController@finish')->name('training.finish');
});
Route::group(['prefix' => '/manage'], function(){
    Route::get('/', 'ManageController@index')->name('manage');
    Route::post('/search', 'ManageController@search')->name('manage.search');
});
Route::group(['prefix' => '/history'], function(){
    Route::get('/', 'HistoryController@index')->name('history');
    Route::post('/show', 'HistoryController@show')->name('history.show');
});
Route::group(['prefix' => '/download'], function(){
    Route::get('', 'DownloadController@index')->name('download');
    Route::get('/create', 'DownloadController@create')->name('download.create');
    Route::post('/store', 'DownloadController@store')->name('download.store');
});
