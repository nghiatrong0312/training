@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <div class="company_title">
                <h3>Tai Xe</h3>
                <form action="" id="form_driver">
                <div class="row">
                    <div class="col-sm-4 company_title-input">
                        <input type="text" class="form-control" id="name" placeholder="Ten Tai Xe" name="name">
                    </div>
                    <div class="col-sm-4 company_title-input" style="text-transform: uppercase">
                        <input type="text" class="form-control" id="bks" placeholder="Bien Kiem Soat" name="bks">
                    </div>
                    <div class="col-sm-4 company_title-input">
                        <select class="form-control" id="company" name="company">
                            <option>Chon Cong Ty</option>
                            @foreach ($data_companies as $data_company)
                            <option value="{{$data_company['id']}}">{{$data_company['name']}}</option>
                            @endforeach
                          </select>
                    </div>
                    <div class="col-sm-4 company_title-button">
                        <button type="submit" id="submit_driver" class="btn btn-secondary">Hoan Tat</button>
                    </div>
                </div>
                </form>
            </div>
            <div class="company_content">
                <table class="table table-dark table-striped">
                    <thead>
                      <tr>
                        <th>Ten Tai Xe</th>
                        <th>BKS</th>
                        <th>Cong Ty</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody id="show_content">
                        @foreach ($datas as $data)
                            <tr>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['bks']}}</td>
                                <td>{{$data->getNameCompany->name}}</td>
                                <td><a href="{{route('driver.delete', ['id' => $data['id']])}}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#submit_driver').click(function(event){
            event.preventDefault();
            $.ajax({
                url:'{{ route('driver.create') }}',
                type:'post',
                dataType: 'html',
                data : {
                    "_token": "{{ csrf_token() }}",
                    name:$('#name').val(),
                    bks:$('#bks').val(),
                    company:$('#company').val(),
                },
            }).done(function(data){
                $('#form_driver')[0].reset();
                $('#name').focus();
                $('#show_content').html(data);
            });
        });
        

    });
</script>
@endsection