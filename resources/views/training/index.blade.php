@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <div class="company_title">
                <h3>Training</h3>
                <form action="{{route('training.finish', ['id' => $id])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                <div class="row">
                    <div class="col-sm-8 company_title-input">
                        <input type="text" class="form-control" id="note" placeholder="Ghi Chu" name="note">
                    </div>
                    <div class="col-sm-8 company_title-button">
                        <button type="submit" id="submit_driver" class="btn btn-secondary">Hoan Tat</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection