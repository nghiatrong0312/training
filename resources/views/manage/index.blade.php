@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="company_title">
                <h3>Manage</h3>
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search" id="name_manage">
                    <div class="input-group-append">
                        <button class="btn btn-success" type="submit" id="search_manage">Go</button>
                    </div>
                </div>
                <table class="table table-dark table-striped">
                    <thead>
                      <tr>
                        <th>Ten</th>
                        <th>BKS</th>
                        <th>Cong Ty</th>
                        <th>Note</th>
                      </tr>
                    </thead>
                    <tbody id="show_data_manage">
                      
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#search_manage').click(function(event){
            event.preventDefault();
            $.ajax({
                url:'{{ route('manage.search') }}',
                type:'post',
                dataType: 'html',
                data : {
                    "_token": "{{ csrf_token() }}",
                    name:$('#name_manage').val(),
                },
            }).done(function(data){
                $('#show_data_manage').html(data);
            });
        });
    });
</script>
@endsection