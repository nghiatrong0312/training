@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <form action="{{route('download.store')}}" enctype="multipart/form-data" method="POST">
            @csrf
            <div class="company_title">
                <h3>Upload</h3>
                <div class="row">
                    <div class="col-sm-8 company_title-input">
                        <input type="file" class="form-control" id="file_pdf" placeholder="Ten Cong Ty" name="file_pdf">
                    </div>
                    <div class="col-sm-4 company_title-button">
                        <button type="submit" class="btn btn-secondary">Hoan Tat</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

@endsection