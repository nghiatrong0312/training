@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if ( Session::has('note_finish') )
                <p style="color:#ff8484" class="help is-danger">{{ Session::get('note_finish') }}</p>
            @endif
            <div class="input-group mb-3">
                <input type="text" class="form-control" id="search_input" placeholder="Bien Kiem Soat...">
                <div class="input-group-append">
                  <button class="btn btn-success" type="submit" id="search">Go</button>
                </div>
            </div>
            <table class="table table-dark table-striped">
                <thead>
                  <tr>
                    <th>Ten</th>
                    <th>BKS</th>
                    <th>Cong Ty</th>
                    <th>Note</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody id="show_search">

                </tbody>
              </table>
        </div>
    </div>
</div>
<script type="text/javascript">
  $(document).ready(function(){
        $('#search').click(function(event){
            $.ajax({
                url:'{{ route('home.search') }}',
                type:'post',
                dataType: 'html',
                data : {
                    "_token": "{{ csrf_token() }}",
                    search:$('#search_input').val(),
                },
            }).done(function(data){
                $('#show_search').html(data);
            });
        });
    });
</script>
@endsection
