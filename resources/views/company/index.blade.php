@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div>
            <form action="{{route('company.create')}}" method="POST">
            @csrf
            <div class="company_title">
                <h3>Cong Ty</h3>
                <div class="row">
                    <div class="col-sm-8 company_title-input">
                        <input type="text" class="form-control" id="name" placeholder="Ten Cong Ty" name="name">
                    </div>
                    <div class="col-sm-4 company_title-button">
                        <button type="submit" class="btn btn-secondary">Hoan Tat</button>
                    </div>
                </div>
            </div>
            </form>
            <div class="company_content">
                <table class="table table-dark table-striped">
                    <thead>
                      <tr>
                        <th>Ten </th>
                        <th>Ngay Tao</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($datas as $data)
                            <tr>
                                <td>{{$data['name']}}</td>
                                <td>{{$data['created_at']}}</td>
                                <td><a href="{{route('company.delete', ['id' => $data['id']])}}"><i class="fa fa-trash-o"></i></a></td>
                            </tr>
                        @endforeach
                       
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
</div>

@endsection