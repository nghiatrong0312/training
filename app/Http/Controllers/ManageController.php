<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Driver;
use App\Company;

class ManageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('manage.index');
    }

    public function search(Request $request)
    {
        $show_data    = '';
        $name_company = $request->name;
        $datas        = Driver::whereHas('getNameCompany', function ($query) use ($name_company) {
            $query->where('companies.name', 'like', '%'.$name_company.'%')
                    ->where('drivers.status', 1);
        })->get();

        foreach ($datas as $key => $data) {
            $show_data .= '
            <tr>
                <td>'.$data['name'].'</td>
                <td>'.$data['bks'].'</td>
                <td>'.$data->getNameCompany->name.'</td>
                <td>'.$data['note'].'</td>
            </tr>
            ';
        }
        return $show_data;
        
    }
}
