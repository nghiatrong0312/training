<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Driver;

class HistoryController extends Controller
{
    public function index()
    {
        return view('history.index');
    }

    public function show(Request $request)
    {
        $show_history = 'Khong Co Du Lieu';
        $id_user      = Auth::id();
        $date         = $request->date; 
        $name_user    = 'SAMSUNG';
        $datas = Driver::whereHas('getHistoryTraining', function ($query) use ($id_user,$date,$name_user) {
            $query->where(['users.id'=> $id_user])
                    ->whereDATE('drivers.updated_at', 'like', '%'.$date.'%');
        })->get();
        foreach ($datas as $key => $data) {
            $show_history .= '
            <tr>
                <td>'.$data['name'].'</td>
                <td>'.$data['bks'].'</td>
                <td>'.$data->getNameCompany->name.'</td>
                <td>'.$data['updated_at'].'</td>
            </tr>
            ';
        }
        if ($datas) {
            return $show_history;
        }
        return $show_history;
    }
}
