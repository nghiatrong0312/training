<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Driver;
use Session;

class TrainingController extends Controller
{
    public function __construct()
    {
        
        $this->middleware('auth');
    }

    public function index($id)
    {
        return view('training/index',[
            'id' => $id,
        ]);
    }

    public function finish(Request $request, $id)
    {
        $user_id = '';
        if (Auth::id()) {
            $user_id = Auth::id();
        }
        $dataUpdate = [
            'status'  => 1,
            'note'    => $request->note,  
            'user_id' => $user_id,
        ];

        Driver::where(['id' => $id])->update($dataUpdate);

        Session::flash('note_finish', 'Training Thanh Cong.');
        return redirect('home');
        
    }
}
