<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use Session;

class DownloadController extends Controller
{
    public function index()
    {
        //PDF file is stored under project/public/download/info.pdf
        return response()->download(storage_path('app/upload/'.'intro.pdf'));
    }

    public function create()
    {
        return view('download.create');
    }
    public function store(Request $request)
    {
        if ($request) {
            $files = $request->file_pdf;
            Storage::put('upload/'.$files->getClientOriginalName(), file_get_contents($files));
            Session::flash('upload_success', 'Upload Thanh Cong.');
            return redirect('');
        }
       

    }
}
