<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

use App\Company;

class CompanyController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $datas = Company::All();
        return view('company/index',[
            'datas' => $datas,
        ]);
    }

    public function create(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name'      => 'required',
        ]);

        if ($validator->fails()) {
            // return redirect('company/index')->withErrors($validator)->withInput();
            echo '123';
        }
        else {
           
            $dataInsert = [
                'name' => $request->name,
            ];
            Company::create($dataInsert);

            return redirect('company/index');
        }
    }

    public function delete($id)
    {
        Company::destroy($id);

        return redirect('company/index');
    }
}
