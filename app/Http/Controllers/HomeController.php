<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Driver;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function search(Request $request)
    {
        $show_data  = 'Khong Co Du Lieu';
        $datas      = Driver::where(['bks' => $request->search])->where(['status' => 0])->orwhere('note', 'like', '%ios%')->where(['bks' => $request->search])->get();
        foreach ($datas as $data) {
            $show_data .= '
            <tr>
                <td>'.$data['name'].'</td>
                <td>'.$data['bks'].'</td>
                <td>'.$data['bks'].'</td>
                <td>'.$data['note'].'</td>
                <td></td>
                <td><a href="'.route('training', ['id' => $data['id']]).'"><button type="button" class="btn btn-success" style="float: right">Training</button></a></td>

            </tr>
            ';
        }
        if ($datas) {
            return $show_data;
        }
        return $show_data;
    }
}
