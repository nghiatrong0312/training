<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

use App\Driver;
use App\Company;

class DriverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        if (Auth::id() != 1) {
            return redirect('');
        }
    }

    public function index()
    {
        $datas          = Driver::All();
        $data_companies = Company::All();
        return view('driver/index',[
            'datas'             => $datas,
            'data_companies'    => $data_companies,
        ]);
    }

    public function create(Request $request)
    {
        if ($request) {

            $dataInsert = [
                'name'       => request('name'),
                'bks'        => request('bks'),
                'company_id' => request('company'),
                'status'     => 0,
            ];
            Driver::create($dataInsert);
        }
        $datas  = Driver::All();
        $show   = '';
        foreach ($datas as $key => $data) {
            $show .= '
            <tr>
                <td>'.$data['name'].'</td>
                <td>'.$data['bks'].'</td>
                <td>'.$data->getNameCompany->name.'</td>
                <td><a href="'.route('driver.delete', ['id' => $data['id']]).'"><i class="fa fa-trash-o"></i></a></td>
            </tr>
            ';
        }
        return $show;
        
    }

    public function delete($id)
    {
        Driver::destroy($id);

        return redirect('driver/index');
    }
}
