<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\User;

class Driver extends Model
{
    protected $table = 'drivers';
    protected $guarded = [];

    public function getNameCompany()
    {
        return $this->belongsto(Company::class, 'company_id', 'id');
    }

    public function getHistoryTraining()
    {
        return $this->belongsto(User::class, 'user_id', 'id');
    }
}
